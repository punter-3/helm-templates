{{/*
Get the ingress port or first TCP port from the values.ports
*/}}
{{- define "ingress.port" -}}
{{- $defaultPort := 8080 -}}
{{- $ingressPort := "" -}}
{{- if .Values.ports }}
  {{- range .Values.ports }}
    {{- if .ingress }}
      {{- $ingressPort = .port -}}
      {{- break -}}
    {{- end -}}
  {{- end -}}
  {{- if not $ingressPort }}
    {{- range .Values.ports }}
      {{- if eq .protocol "TCP" }}
        {{- $ingressPort = .port -}}
        {{- break -}}
      {{- end -}}
    {{- end -}}
  {{- end -}}
{{- end -}}
{{- $ingressPort | default $defaultPort -}}
{{- end -}}
